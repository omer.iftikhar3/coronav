import React, { Component } from 'react';
import loader from '../res/loader.gif'

class Loader extends Component {

    render(){
        return (
            <div style={{display: "flex",
                        flex: 1,
                        justifyContent: "center",
                        alignItems: "center"
                    }}
            >
                <img src={loader} alt="Loading..."/>
            </div>
            
        )
    }
}

export default Loader;