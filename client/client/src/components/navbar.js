import React, { Component } from 'react';

class Navbar extends Component {

    render(){
        return (
            <div class="navbar-fixed">
                <div class="row">
                    <div class="col s12 center" style={{
                                                left: 0,
                                                width: '100%',
                                                background: '#005d66',
                                                padding: '5px 0',
                                                marginBottom: '10px'
                                            }}>
                    <p style={{color: 'white'}}>Dial <b>1166</b> health helpline</p>
                    <h3 style={{color: 'white'}}><b>STAY HOME STAY SAFE</b></h3>
                    </div>
                </div>
                <img src="http://covid.gov.pk/theme_img/core-img/Corona-Logo2.gif" alt="Corona Virus"></img>
            </div>
        )
    }
}

export default Navbar;
      