import React, { Component } from 'react';

class Stat extends Component {

    render(){
        return (
            <div class="card horizontal" style={{backgroundColor: this.props.cardColor}}>
            <div class="card-stacked">
              <div class="card-content">
                <div>
                  <h4><b style={{color: this.props.textColor ? this.props.textColor : null}}>{this.props.value}</b></h4>
                  <h6>
                    <b style={{color: this.props.textColor ? this.props.textColor : null}}>{this.props.name}</b>
                  </h6>
                </div>
                <div>
                <i class={this.props.icon} style={{color: this.props.textColor ? this.props.textColor : null,
                                                    fontSize: '40px'}}>
                </i>
                </div>
              </div>
            </div>
          </div>
        )
    }
}

export default Stat;