import React, { Component } from 'react';
import M from 'materialize-css/dist/js/materialize.min.js'
import axios from 'axios';

class EmailInput extends Component {

    constructor(props) {
        super(props);
        this.state = {email: null};
    }

    handleChange = event => {this.setState({email: event.target.value});}

    submit_form = event => {
        event.preventDefault();
        var email = this.state.email;
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (re.test(String(email).toLowerCase())) {
            // Save email in databse
            axios.post("http://localhost:4999/user", {email: email});
            M.toast({html: 'Email added  <i class="material-icons green">check</i>', classes: 'rounded'})
            this.setState({email: ''})
        }
        else {
            M.toast({html: 'Email Invalid  <i class="material-icons red">close</i>', classes: 'rounded'})
        }

    }

    render(){
        return (
            <div>
                <form onSubmit={this.submit_form}>
                    <b><i>Enter email to get notified whenever stats are updated </i></b>
                    <div class="input-field inline">
                        <input id="email_inline" type="email" onChange={this.handleChange} value={this.state.email}/>
                    </div>
                    <button class="btn waves-effect waves-light" type="submit" name="action">Submit
                        <i class="material-icons right">send</i>
                    </button> 
                </form>
            </div>
        )
    }
}

export default EmailInput;