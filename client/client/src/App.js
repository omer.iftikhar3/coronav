import React, { Component } from 'react';
import axios from 'axios';
import Loader from './components/loader'
import Stat from './components/stat'
import Navbar from './components/navbar'
import EmailInput from './components/email'

class App extends Component {

  state = {}

  componentDidMount() {
    axios.get('http://localhost:4999/hello')
      .then(res => {
        const stats = res.data;
        this.setState({stats})
      })
  }

  render() {
    return (

      this.state.stats
      ?
      <div class="container-fluid">
        {/* TOP NAV BAR */}
        <Navbar/>

        {/* Email */}
        <div class="row">
          <div class="col s12 center">
            <h1><b>PAKISTAN COVID-19 STATS</b></h1>
            <EmailInput/>
          </div>
        </div>

        {/* TOTAL CASES */}
        <div class="row">
          <div class="col s6 offset-s3 center">
            <Stat value={this.state.stats['total_cases']}
                  name="TOTAL CASES"
                  textColor="#00adef"
                  cardColor="#dde9fd"
                  icon="fa fa-users"/>
          </div>
        </div>


        {/* DEATHS & RECOVERIES */}
        <div class="row">
          <div class="col s3 center">
            <Stat value={this.state.stats['deaths']}
                  name="TOTAL CASES"
                  textColor="#E63C38"
                  cardColor="#F4C7C2"
                  icon="fa fa-bed"/>
          </div>

          <div class="col s3 center">
            <Stat value={this.state.stats['recent_deaths']}
                  name="DEATHS (24 Hours)"
                  textColor="#E12B27"
                  cardColor="#F5C8C8"
                  icon="fa fa-heartbeat"/>
          </div>

          <div class="col s3 center">
            <Stat value={this.state.stats['recent_cases']}
                  name="CASES (24 Hours)"
                  textColor="#FB9005"
                  cardColor="#FFECB2"
                  icon="fa fa-ambulance"/>
          </div>

          <div class="col s3 center">
            <Stat value={this.state.stats['recovered']}
                  name="RECOVERED"
                  textColor="#4DA751"
                  cardColor="#C6E4C8"
                  icon="fa fa-child"/>
          </div>
        </div>

        {/* PROVINCES */}
        <div class="row">
          <div class="col s3 center">
            <Stat value={this.state.stats['punjab_cases']}
                  name="PUNJAB"
                  textColor="#665bcf"
                  cardColor="#afabd6"
                  icon="fa fa-map-marker"/>
          </div>

          <div class="col s3 center">
            <Stat value={this.state.stats['sindh_cases']}
                  name="SINDH"
                  textColor="#665bcf"
                  cardColor="#afabd6"
                  icon="fa fa-map-marker"/>
          </div>

          <div class="col s3 center">
            <Stat value={this.state.stats['kpk_cases']}
                  name="KPK"
                  textColor="#665bcf"
                  cardColor="#afabd6"
                  icon="fa fa-map-marker"/>
          </div>

          <div class="col s3 center">
            <Stat value={this.state.stats['balochistan_cases']}
                  name="BALOCHISTAN"
                  textColor="#665bcf"
                  cardColor="#afabd6"
                  icon="fa fa-map-marker"/>
          </div>
        </div>

        <div class="row">
          <div class="col s5 offset-s7">
            <p class="right">Last updated at: {this.state.stats['stats_time']}</p>
            <p class="right"
                style={{color: 'red',
                        fontSize: '9px'}}>
                *Dashboard numbers are updated as soon as authenticated test results are received from the laboratories.
            </p>
          </div>
        </div>

      </div>
      :
      <Loader/>
      
    );
  }
}

export default App;
