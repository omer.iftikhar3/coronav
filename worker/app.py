from flask import Flask, request
from flask_cors import CORS, cross_origin
import os
import json
import data.user.user as user_store

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'


@app.route('/hello', methods=["GET"])
def scrape_stats():
    """
    Runs scrappy spider to scrape latest stats
    """
    stats_file = './coronav_scrapper/stats.json'
    if os.path.exists(stats_file):
        os.unlink(stats_file)

    spider_name = 'coronav_scrapper'
    os.chdir('./coronav_scrapper')
    os.system('scrapy crawl coronav_scrapper -o stats.json')
    os.chdir('./../')
    stats = None
    with open(stats_file) as file:
        stats = json.load(file)[0]
    return stats


@app.route('/user', methods=['POST'])
def add_user():
    req = request.get_json()
    if not req.get('email'):
        return "Email not found."
    
    user_store.add_user(req['email'])
    return "success"


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=4999)