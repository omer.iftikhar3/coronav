from db.db import baseDatabase as DB

def add_user(email):
    """
    Adds user
    """
    query = """
        insert into users (user_email)
        values
        (%s)
        on conflict do nothing;
    """
    with DB() as db:
        curs = db.cursor()
        curs.execute(query, [email])
