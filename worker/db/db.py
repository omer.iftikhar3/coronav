"""
Base database class
"""

import psycopg2
import json

with open('./db/db_creds.json') as file:
    creds = json.load(file)

class baseDatabase():
    """
    Base database class
    """

    def __init__(self):
        print("Constructor called")
        self.conn = None

    def __enter__(self):
        self.conn = psycopg2.connect(user=creds['POSTGRES_USER'], password=creds['POSTGRES_PASSWORD'],
                                     host=creds['POSTGRES_HOST'], port=creds['POSTGRES_PORT'],
                                     database=creds['POSTGRES_DB'])
        print("Connection created successfully")
        return self.conn

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.conn.commit()
        self.conn.close()
        print("Connection closed succesfully")