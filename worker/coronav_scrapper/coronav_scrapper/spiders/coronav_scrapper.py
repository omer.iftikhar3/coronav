import scrapy
from ..items import CoronavScrapeItem

class coronavScrapper(scrapy.Spider):
    """
    Corona stats scrapper spider class
    """
    # Below variable names must be same as Scrappy expects them to be this.

    # name of scrapper
    name = 'coronav_scrapper'

    #urls to be scrapped 
    start_urls = ['http://covid.gov.pk/']

    def parse(self, response):
        total_cases = response.css('.numanimate::text').extract_first()
        stats_time = response.css('div#date::text').extract_first().strip().strip(' ')
        stats_time = stats_time[stats_time.index(':') + 2:]
        deaths = response.css('div.covid-number-box.deaths h3::text').extract_first()
        punjab_cases = response.css('.grey-border+ .col-sm-6 .covid-number-box+ .covid-number-box h3::text').extract_first()
        sindh_cases = response.css('.grey-border+ .col-sm-6 .covid-number-box:nth-child(1) h3::text').extract_first()
        kpk_cases = response.css('.col-sm-6:nth-child(8) a h3::text').extract_first()
        balochistan_cases = response.css('.grey-border~ .col-sm-6+ .col-sm-6 .covid-number-box+ .covid-number-box a h3::text').extract_first()
        ajk_gb_cases = '/'.join([response.css('h3 .ajk-stats::text').extract_first(), response.css('h3 .stats-gb::text').extract_first()])
        total_tests = response.css('.deaths~ .text-center+ .text-center .text-muted::text').extract_first()
        recent_deaths = response.css('.deaths .text-muted::text').extract_first()
        recent_cases = response.css('.text-center:nth-child(1) .text-muted::text').extract_first()
        recovered = response.css('.revovered h3::text').extract_first()

        corona_stats = CoronavScrapeItem()

        corona_stats['total_cases'] = total_cases
        corona_stats['stats_time'] = stats_time
        corona_stats['deaths'] = deaths
        corona_stats['punjab_cases'] = punjab_cases
        corona_stats['sindh_cases'] = sindh_cases
        corona_stats['kpk_cases'] = kpk_cases
        corona_stats['balochistan_cases'] = balochistan_cases
        corona_stats['ajk_gb_cases'] = ajk_gb_cases
        corona_stats['total_tests'] = total_tests
        corona_stats['recent_deaths'] = recent_deaths
        corona_stats['recent_cases'] = recent_cases
        corona_stats['recovered'] = recovered

        yield corona_stats
