# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class CoronavScrapeItem(scrapy.Item):
    total_cases = scrapy.Field()
    stats_time = scrapy.Field()
    punjab_cases = scrapy.Field()
    sindh_cases = scrapy.Field()
    kpk_cases = scrapy.Field()
    balochistan_cases = scrapy.Field()
    ajk_gb_cases = scrapy.Field()
    deaths = scrapy.Field()
    total_tests = scrapy.Field()
    recent_deaths = scrapy.Field()
    recent_cases = scrapy.Field()
    recovered = scrapy.Field()
